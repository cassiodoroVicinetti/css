\chapter{Sicurezza delle applicazioni Web}
\label{cap:10}
Alcuni dei problemi riscontrabili nello sviluppo di applicazioni web sono:
\begin{itemize}
\item \textit{SQL injection}, ossia l’esecuzione di query arbitrarie sul DB del server.
\item \textit{Cross-site scripting}, ossia l’accesso ai dati conservati nei cookie.
\item \textit{Accesso a dati riservati}, che tipicamente è un problema della gestione delle sessioni e più in generale di gestione nello stato del web.
\item \textit{Buffer overflow}, ossia l’esecuzione di codice arbitrario sul server (ed a volte anche sul client).
\end{itemize}
Tutti i problemi sopra citati sono tipicamente possibili perché nello sviluppare le applicazioni i developers tendono a fidarsi del client. In realtà questo è sbagliato, non bisognerebbe mai fidarsi del client perché controllare il client è assolutamente impossibile: il client è nelle mani dell’utente, che potrebbe potenzialmente essere un attaccante. Questo concetto è valido anche in una Intranet e anche per applicazioni protette da un firewall (il firewall protegge dove chiude le porte ma non dove le apre).

In generale quindi quando si sviluppa un’applicazione web accorrerebbe non fidarsi mai del codice che viene eseguito sul client, e soprattutto occorre sempre assucmere che i dati passati al client possano essere manipolati in modo inatteso. 

In altre parole la sicurezza deve essere server-based, il client è tipicamente una fonte di insicurezza.

Inoltre visto che i dati vengono trasmessi usando una rete non sicura, durante la trasmissione in rete potrebbe capitare qualunque cosa. È quindi essenziale \underline{validare sempre i dati di provenienza non fidata}, ossia validare i dati anonimi o dati che possono essere stati manipolati o falsificati.

Per la validazione dei dati è possibile seguire due tipi di approcci differenti:
\begin{itemize}
\item \textit{Check that it looks good (whitelist)}, ossia andare a guardare se I dati corrispondono a quello che il server ha come concetto di dati validi.
\item \textit{Don’t match that it looks bad (blacklist)}, ossia I dati non fanno match con quello che si sa essere un attacco.
\end{itemize}
La soluzione da preferire è quella della whitelist, perché andare a prevedere tutti i possibili modi con cui i dati possano essere alterati per realizzare un attacco è estremamente difficile. Dati non validati o non ripuliti prima di essere accettati dal server sono la sorgente di moltissimi attacchi.

\section{SQL Injection}
La SQL injection significa essenzialmente fornire un input artefatto per alterare il codice SQL generato dinamicamente da un server. In particolare l’attacco consiste in:
\begin{itemize}
\item Modificare le condizioni di una query.
\item Selezionare dati fuori dalla tabella che teoricamente si stava usando.
\end{itemize}
Questo attacco viene usato per molti scopi, ma molto spesso per rubare le credenziali di autenticazione di un utente. Affinchè l’attacco vada a buon fine potrebbe essere necessario l’utilizzo di una qualche forma di social engineering.


\subsection{Esempio JSP n.1}
\begin{figure}[H]
 \centering
 \includegraphics[scale=0.725]{pic/10/1}
\end{figure}
 
Nell’esempio si ha una query in cui si va a selezionare tutte le righe dalla tabella WebUsers, dove lo username corrisponde a quello introdotto nel campo “username” del form di autenticazione, e la password corrisponde a quella introdotta nel campo “password”.
Nelle due righe successive si prepara lo statement SQL e se ne fa l’esecuzione.

\begin{figure}[H]
 \centering
 \includegraphics[scale=0.725]{pic/10/2}
\end{figure}
 
Nell’immagine sovrastante si ho un esempio di uso normale della procedura.

\begin{figure}[H]
 \centering
 \includegraphics[scale=0.725]{pic/10/3}
\end{figure}
 
In questo caso invece un possibile attaccante esegue come password del codice opportuno, in modo che la query fornisca sempre un risultato positivo indipendentemente dal fatto che la password e lo username siano giusti.

\subsection{Esempio JSP n.2}

\begin{figure}[H]
 \centering
 \includegraphics[scale=0.725]{pic/10/4}
\end{figure}

\begin{figure}
\centering
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[width=\linewidth]{pic/10/5a}
	\end{subfigure}%
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[width=\linewidth]{pic/10/5b}
	\end{subfigure}
\end{figure}

Grazie al fatto che non c’è nessun controllo sui nomi inseriti dall’utente, anche in questo caso l’attaccante riuscirà ad avere accesso ad informazioni riservate semplicemente andando ad inserire del codice ben studiato.

\subsection{SQL Injection – Come evitarlo}
Per cercare di evitare questo tipo di attacco esistono una serie di linee guida da seguire:
\begin{itemize}
\item Revisionare tutti gli script e tutte le pagine dinamiche, comunque generate (non solo quelle generate da JSP, ma anche quelle generate con PHP, ASP, …).
\item Validare l’input degli utenti, trasformando gli apici singoli in sequenze di due apici.
\item Suggerire agli svilupaptori di usare le query parametrizzate (query SQL la cui struttura non può essere cambiata dai dati che vengono introdotti) per introdurre i valori delle variabili fornite dall’utente, piuttosto che generare codice SQL tramite concatenazione di stringhe.
\item Usare software di testing per verificare la propria vulnerabilità a questo tipo di attacco.
\end{itemize}

\subsection{SQL Injection – Dove può capire?}
La SQL injection può capitare non solo nelle query dinamiche generate a partire da dei form web, ma anche in altri casi dove si va a creare delle query con dei parametri. Ad esempio nei Java Servlets, nelle Java Stored Procedures, in generale nei web services (SOAP, …). 

In generale quindi è possibile subire questo attacco in tutti quegli ambiti in cui si va a costruire un’azione a partire da dati ricevuti dall’utente.

\section{Cross-site scripting}
Anche noto come XSS (talvolta anche CSS), è un attacco usato per vari scopi, spesso per rubare le credenziali di autenticazione di un utente. 

Affinchè l’attacco vada a buon fine è necessario usare una qualche forma di social engineering. È molto difficile da contrastare perché usa una grande varietà di meccanismi, ed è poco compreso dagli sviluppatori applicativi (anche data la complessità delle attuali applicazioni web). È un attacco molto più comune di quanto si pensi.

\subsection{XSS – Come funziona}
L’attaccante identifica un sito web che non filtra i tag HTML quando accetta input da un utente, così da andare ad inserire del codice HTML e/o degli script arbitrari in un link o in una pagina.

Uno degli esempi più clamorosi di questo attacco è quello che ha riguardato Ebay. L’attaccante andava a registrare un oggetto su Ebay ed andava ad incorporare lo script nella sua descrizione. Quando un utente andava a visualizzare la pagina relativa all’offerta, lo script veniva eseguito; lo script approfittava del fatto che l’utente era collegato con il cookie di Ebay per andare a fare un’offerta all’oggetto dell’inserzione senza che l’utente se ne accorgesse.

Questo attacco serve ad evitare la cosiddetta \textit{Same-Origin Policy}, policy secondo la quale i browser permettono agli script del sito X di accedere solo ai cookie e ai dati provenienti dal sito X. Per tale ragione si fa eseguire alla vittima uno script per far inviare i suoi cookie (relativi al sito dello script) al sito web dell’attaccante.
 
\begin{figure}[H]
 \centering
 \includegraphics[scale=0.725]{pic/10/6}
\end{figure} 

Questo tipo di attacco è molto difficile da rilevare perché lo script può essere:
\begin{itemize}
\item Lasciato in chiaro.
\item Offuscato (es. codifica esadecimale della stringa).
\item Generato al volo ad esempio tramite un altro script.
\item Nascosto dentro un messaggio di errore.
\end{itemize}

\subsection{Sequence diagram}
 
\begin{figure}[H]
 \centering
 \includegraphics[scale=0.75]{pic/10/7}
\end{figure} 

\begin{enumerate}
\item L’attaccante costruisce un link malizioso.
\item Fase di social enginnering, in cui l’attaccante invita la vittima a visitare il link precedentemente creato.
\item L’utente richiede la pagina.
\item Ottiene la pagina che contiene lo script malizioso.
\item Lo script viene eseguito.
\item Esecuzione di una qualche richiesta, di cui l’utente non è conscio.
\end{enumerate}

\subsection{XSS – Tipologie di attacco}
\subsubsection{Persistent XSS (stored XSS)}
In questa versione dell’attacco l’attaccante usa un browser per inviare dei dati (es. tramite un form), che vengono memorizzati permanentemente sul server e poi essere inviati o resi disponibili ad altri utenti.

Il problema in questo caso è che i dati contengono tag HTML (soprattutto tag <script>) che modificano il contenuto della pagina.

Esempio di attacco è un post di un commento che contiene uno script per fare redirect verso un altro sito.

Nel 2008 una vulnerabilità di tipo persistent XSS su MySpace, unita con un worm, diffuso tramite un filmato QuickTime ha permesso l’esecuzione di codice JSP arbitrario usato per rubare le credenziali (phishing) ed il listing del contenuto del filesystem di una determinata persona.

\subsubsection{Non-persistent XSS (reflected XSS)}
L’attaccante invia dei dati tramite un browser, che vengono direttamente usati nell’applicazione lato server (es. in una query SQL) per generare la risposta. Si ha quindi il problema di generare dell’input inatteso all’applicazione, che va ad eseguire un’operazione imprevista.

SQL injection è un esempio di non-persistent XSS, in quanto questo attacco non va a modificare permanentemente il server, ma va a modificare il formato dell’operazione della query SQL.

\subsubsection{DOM XSS}
L’attaccante invia dei dati tramite un browser, che vengono memorizzati o usati direttamente dal server per generare la risposta.

Il problema è che i dati contengono dei tag che manipolano il DOM (Document Object Model), ad esempio usando una XmlHttpRequest, che provocano un compartamento inatteso lato client.

\subsection{XSS – Contromisure}
In generale se si vuole evitare di subire degli attacchi XSS occorre:
\begin{itemize}
\item Sempre validare l’input, andando ad accettare solo l’input corretto e rifiutare (MAI cercare di correggere) input errato.
\item Codificare l’output con le entità HTML; ad esempio se si vuole inserire l’apice si utilizzi ‘‘\&quot’’, che graficamente rappresenta l’apice ma non è interpretabile come un tag SQL. Non bisogna quindi mai usare codici ASCII o UTF-8.
\item Specificare sempre la codifica dell’output per evitare interpretazioni errate.
\item Non usare mai le blacklist ma sempre le whitelist, principalmente perché p difficile scrivere correttamente una blacklist che tenga conto di tutti i possibili tipi di attacco.
\item  Canonicalizzare l’input prima di esaminarlo, questo per evitare errori di interpretazione.
\end{itemize}

\subsection {XSS – Alcuni controlli base}
Innanzitutto esistono librerire anti-XSS, specifiche dei vari linguaggi di programmazione. Ad esempio sia in .NET che in PHP quando si riceve un input è possibile chiamare una funzione di un’apposita libreria che controlla se l’input nasconda qualcosa di malevolo. Questo tipo di controllo è per i “pigri”, in quanto si basa su una blacklist: vengono ricercati se i dati ricevuti contengono delle informazioni associabili ad un attacco XSS.

L’idea migliore sarebbe quello di creare una whitelist (tipicamente un’espressione regolare) per ogni valore di input che si riceve.

Un’altra soluzione è quella di ripulire l’input ricevuto, andando a filtrare i metacaratteri, e andare a convertire qualunque testo salvato o letto.
                                                
\begin{figure}[H]
 \centering
 \includegraphics[scale=0.725]{pic/10/8}
 \caption{Se l'utente ha inserito un carattere che potrebbe essere pericoloso, è buona norma andare a sostituirlo con un'entità HTML che risulta non essere pericolosa.}
\end{figure} 

\section{Buffer overflow}
In questo tipo di attacco si cerca di fornire più dati del massimo atteso per superare i limiti della memoria allocata e far eseguire codice arbitrario.

Buffer overflow, integer overflow o errori nelle stringhe di format sono molto importanti per programmi scritti in C/C++, quali ad esempio le applicazioni custom o I server web stessi.

Per capire come funziona il buffer overflow bisogna ricordarsi il funzionamento dello stack. Ad ogni chiamata di procedura/funzione:
\begin{itemize}
\item Si salva nello stack l’indirizzo di ritorno;
\item Si alloca sullo stack le variabili necessarie alla funzione.
\end{itemize}
 
\begin{figure}[H]
 \centering
 \includegraphics[scale=0.725]{pic/10/9}
\end{figure} 

\subsection{Buffer overflow – Esempio}
 
\begin{figure}[H]
 \centering
 \includegraphics[scale=0.725]{pic/10/10}
\end{figure} 

Questo è la tipologia di attacco che si è avuta nei confronti dell’IS, in cui se si aveva una stringa nella URL più lunga di 256 caratteri si andavano a sovrascrivere i dati presenti in memoria ed era possibile eseguire del codice arbitrario.

\subsection{Buffer overflow – Contromisure}
Le contromisure principali per il buffer overflow sono:
\begin{itemize}
\item Buona programmazione, andando a non usare ad esempio funzioni come strcpy, strcat, … ma strncpy, strncat, …
\item Usare sistemi automatici di protezione
\begin{itemize}
\item Modificare il compilatore per proteggere l’indirizzo di ritorno mettendogli attorno dei “canarini”, ossia due stringhe che vengono messe subito prima e subito dopo l’indirizzo di ritorno. Se quando la procedura finisce i canarini non sono più vivi (non hanno più lo stesso valore iniziale), è un’indicazione che sono stati sovrascritti e occorrerà bloccare l’esecuzione del programma.
\item Configurare lo stack come “no exec” (default il Solaris 9).
\end{itemize}
Esistono tuttavia dei nuovi metodi di attacco che sono in grado di aggirare queste protezioni, adando ad esempio a cambiare l’indirizzo di ritorno con l’indirizzo che attiva una DLL. La soluzione migliore quindi è quella di scrivere bene il codice.
\item Esaminare i log per cercare tentativi di attacco.
\end{itemize}

\section{OWASP top 10 web risks (2013)}
OWASP è un progetto completamente OpenSource che mira a sviluppare tecniche, soluzioni ma anche solo conoscenza circa la sicurezza delle applicazioni web. Tra le tante iniziative di questo progetto si ha la testura periodica di una lista dei 10 attacchi più frequenti.
 
\begin{figure}[H]
 \centering
 \includegraphics[scale=0.725]{pic/10/11}
\end{figure} 

\subsection{Injection}
SQL injection è sicuramente il problema più famoso, ma sono possibili anche altri tipi di attacco in quanto in generale le applicazioni web usano la costruzione di stringhe per molte cose. È quindi possibile eseguire attacchi tramite LDAP (usa una stringa per fare una query all’interno del directory), XPath (per selezionare parti di un DOM), XSLT, HTML, CML, OS command, …

Problema: avere dell’input inatteso eseguito da un interprete.

Soluzioni:
\begin {itemize}
\item Evitare (per quanto possibile) gli interpreti.
\item Usare comandi a struttura fissa, usando l’input dell’utente solo come un parametro.
\end{itemize}

\subsection{Broken authentication and session management}
Al secondo posto si hanno problemi riguardanti l’autenticazione e la gestione delle sessioni, principalmente dovuti al fatto che si utilizzano schemi non standard di autenticazione che contengono spesso degli errori. Progettare degli schemi di autenticazione in maniera corretta è molto difficile. Le aree più comuni dove è possibile avere degli errori sono il log-out, la gestione delle password, timeout, la funzione “ricordami su questo computer” e l’aggiornamento dell’account.

Tutti questi problemi causano l’esposizione delle password o degli account e degli identificativi di sessione, permettendo di ottenere i privilegi della vittima e spesso di controllare molte altre sessioni.

Sono in generale attacchi difficili da individuare perché utilizzano molte tecniche diverse.

Questi attacchi potrebbero in generale essere contrastati andando ad usare degli schemi di autenticazione standard, cercando di avere cura dei cookie di sessione andando ad eliminarli non appena non sono più utilizzati.

\subsection{Insecure direct object references}
Può capitare che le applicazioni usino direttamente come parametro un oggetto reale (es. una chiave di DB, un nome di un file), andando ad esporre questa informazione all’utente. Il client potrebbe andare a cambiare tale riferimento per cercare di accedere ad un diverso oggetto.

Ad esempio nel 2000 il server web dell’ufficio Australiano delle tasse usava nella URL il codice fiscale dell’utente. Sfruttando questa anomalia un utente, autenticato con le sue credenziali, è riuscito a raccogliere i dati fiscali di altri 17000 utenti. Questo attacco è stato possibile perché non c’era nessuna correlazione tra i dati di autenticazione e il codice fiscale, non aveva nessun senso usare come parametro il codice fiscale.

Soluzione: Non esporre mai nell’interfaccia accessibile all’utente i riferimenti agli oggetti applicativi

\subsubsection{Esempio}
Query SQL composta con input fornito dall’utente.
 
\begin{figure}[H]
 \centering
 \includegraphics[scale=0.725]{pic/10/12}
\end{figure}

Cambiando il parametro “acct” dal proprio browser l’attaccante può accedere a qualsiasi altro account.

\subsubsection{Security misconfiguration}
Se un sistema non è stato configurato a dovere, un attaccante potrebbe essere in grado di accedere al sistema o di raccogliere informazioni riservate. Tipici problemi di malconfigurazione sono:
\begin{itemize}
\item Uso di credenziali di default.
\item Pagine non più usate ma che non sono state rimosse dal server.
\item Nessuna precauzione contro vulnerabilità note.
\item File e cartelle non protetti; solo per il fatto che qualcosa non è esposto non significa che non sia accessibile.
\end{itemize}
Errori di configurazione possono presentarsi a qualunque livello dello stack applicativo. Questo significa che gli sviluppatori devono avere una stretta collaborazione con gli amministratori di sistema, in modo da assicurarsi che l’intero stack sia correttamente configurato, andando anche ad usare degli scanner automatici.

\subsubsection{Scenari}
Possibili scenari di attacco possono essere:
\begin{itemize}
\item Utilizzo di framework vulnerabili ad attacchi XSS, magari a causa dell’uso di librerie non aggiornate.
\item Account di amministrazione creato automaticamente, usando username e password standard.
\item Directory listing non disabilitato, che potrebbe essere sfruttato da un attaccante per scaricare tutto il codice sorgente ed identificare le vulnerabilità più facilmente.
\item Visualizzazione da parte del server web dello stack trace in caso del verificarsi di qualche errore, che potrebbe essere sfruttatto da un attaccante per ricavare informazioni utili.
\end{itemize}
 
\subsection{Sensitive data exposure}
I dati sensibili vanno protetti in modo particolare sia quando memorizzati permanentemente sia in trasmissione. Si parla in particolare di data at rest, ossia memorizzati sull’unità di memoria, e di data in transit, quando i dati vengono trasmessi.

Tipici problemi per i data at rest:
\begin{itemize}
\item Dati sensibili non cifrati o cifrati con algoritmi “casalinghi”.
\item Uso errato di algoritmi forti.
\item Uso di algoritmi notoriamente deboli, come RC2-40.
\item Chiavi memorizzate direttamente nell’applicazione o in file non protetti.
\item Memorizzare dati non necessari. Bisognerebbe cercare si seguire sempre il principio del need to know, ossia memorizzare solo i dati che servono veramente per l’applicazione che si sta eseguendo.
\end{itemize}
Per quanto riguarda la parte di data in transit, è necessario andare a considera l’uso solo di canali protetti, sia per le sessioni autenticate verso i client, sia per i collegamenti verso il back-end.

Spesso non viene fatto l’uso di un canale sicuro per un presunto peggioramento delle prestazioni (in parte questo è vero), o per conflitti con gli IDS/IPS. Spesso SSL/TLS è usato solo durante la fase di autenticazione, o con certificati scaduti o non configurati correttamente.

\subsubsection{Scenari (Data at rest)}
\begin{itemize}
\item Cifratura dei dati delle carte di credito quando si va a memorizzarli all’interno di un DB, andando però ad utilizzare delle query li decifrano. Un attaccante può quindi ottenere i dati in chiaro tramite un attacco SQL injection. La soluzione più ovvia è quella di non usare mai query che decifrano i dati.
\item Backup su CD di alcuni dati sanitari, andando però ad inserire la chiave sullo stesso backup. Per decifrare i dati è quindi sufficiente entrare in possesso del supporto ottico.
\item Creare il DB delle password senza usare il salt. Ad esempio decifrare il file /etc/passwd con un attacco brute force richiede 4 settimane, invece che 3000 anni!
\end{itemize}

\subsubsection{Scenari (Data in transit)}
\begin{itemize}
\item Siti che non proteggono bene la pagina che richiede l’autenticazione, rendendo la vita molto semplice all’accante che dovrà semplicemente intercettare il traffico di rete, andando ad intercettare il cookie di sessione per impersonare la vittima.
\item Sito che utilizza un certificato scaduto. L’utente è abituato all’avvertimento “Attenzione il certificato è scaduto, vuoi procedere comunque?”, e non si accorge nel momento in cui viene rediretto su un sito diverso e gli comunica le sue credenziali (phishing).
\item Connessione a DB tramite oggetti ODBC/JDBC è molto perisoloso perché tutte le comunicazioni avvengono in chiaro.
\end{itemize}

\subsection{Missing function-level access control}
Questo problema riguarda la mancanza di controlli di accesso a livello della singola funzione. In particolare la protezione di una URL, o di un’azione richiesta tramite una URL (es. ?action=payment), viene fatta semplicemente non rendendola pubblica o non presentandola nell’interfaccia (implementazione del concetto security through obscurity), non andando a fare nessun controllo reale di accesso o andando a fare un semplice controllo client-side o un controllo basato solo su i dati forniti dal client. Questo rende i dati facilmente accessibili a tutti.

Problema: protezione facilmente superabile. 

\subsection{Scenari}
Si supponga di avere le pagine\\
\centerline{http://example.com/app/getappInfo}\\
\centerline{http://example.com/app/admin\_getappInfo}

La prima fornisce informazioni di base, mentre la seconda dovrebbe essere riservata agli amministratori di sistema.

getappInfo è disponibile a tutti, ma il link che porta a admin\_getappInfo richiama una procedura di autenticazione. Tuttavia la procedura di autenticazione non viene attivata se si scrive direttamente la URL nel browser,e quindi è sufficiente indovinare il nome del file per superare l’accesso.

\subsection{Cross-Site Request Forgery (CSRF)}
Questo attacco consiste nell’inviare al browser una URL attiva (contenuta ad esempio in un sito web cattivo o in una mail HTML) per eseguire un’azione sul server bersaglio. 

\begin{figure}[H]
 \centering
 \includegraphics[scale=0.725]{pic/10/14}
\end{figure}
 
L’attacco è estremamente efficace verso quei server web che usano autenticazione automatica, ad esempio basata su cookie dopo login, su ticket di Kerberos o su l’invio automatico di username e password.

\subsection{Using components with known vulnerabilities}
Oggi le applicazioni web sono molto complesse e usano molti componenti, come librerie per trattare i dati lato server o lato client, framework, servizi, ecc.

Le vulnerabilità di queste componenti sono spesso note e risolte ma raramente gli sviluppatori aggiornano le applicazioni (re-build), anche perché spesso non sanno quali librerie stanno usando a causa di dipendenze profonde e nascoste. Questo permette l’uso di strumenti di attacco automatici, ed estende la platea degli attaccanti anche a persone senza le necessarie conoscenze.

Occorre quindi implementare un adeguato Component Lifecycle Management (CLM): bisogna sapere tutti i componenti che si stanno usando, tenere traccia delle loro vulnerabilità, e ogni volta che esce fuori un aggiornamento occorre fare il re-build di tutte le applicazioni che usavano quel determinato componente.

\subsection{Unvalidated redirects and forwards}
Questo attacco consiste nel forzare la vittima ad usare un link con un redirect non validato. Se il link appartiene a un sito valido la vittima si fida. Ma in realtà la pagina bersaglio è specificata in un parametro non validato, e permette quindi di scegliere arbitrariamente la pagina di destinazione.

Questo attacco è molto facile da rilevare se si ha un controllo su i redirect, ma non è così semplice se si usano i forward (uso interno allo stesso sito).

\subsubsection{Scenari}
Si supponga di avere un sito con una funzione “redirect.jsp”, che riceve un singolo parametro “url” contenente l’indirizzo della prossima pagina da eseguire. L’attaccante crea un URL malevolo per installare malware o per fare phishing\\
\centerline{http://www.x.com/redirect.jsp?url=evil.com}

Se la funzione redirect.jsp non contiene nessun controllo, la vittima che riceverà l’URL malevolo e la eseguirà verrà rediretto verso una destinazione non validata.

Un sito potrebbe anche usare la funzione di forward per girare richieste tra diverse parti del sito tramite un parametro (es. se una transazione è avvenuta correttamente o meno).

L’attaccante sfrutta questo a suo vantaggio andando a creare una URL per accedere ad una pagina alla quale non potrebbe accedere direttamente\\
\centerline{http://www.x.com/boring.jsp?fwd=admin.jsp}

Nell’esempio l’attaccante fa forward sulla pagina di admin. Normalmente non avrebbe le credenziali per accedervi, ma se il motore non fa nessun controllo tra la sua identità e le pagine a cui può avere accesso riuscirà ugualmente ad accedervi.